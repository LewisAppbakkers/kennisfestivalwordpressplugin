<?php

/*
Plugin Name: Appbakkers Festival planner
Plugin URI: https://appbakkers.nl/
Description:
Version: 1.0.2
Author: Robin Speerstra, Lewis Clement
Author URI: https://appbakkers.nl/

*/

if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}


add_action( 'rest_api_init', function () {
    register_rest_route( 'tegels', 'all', array(
        'methods' => 'GET',
        'callback' => 'get_tegels',
    ) );
    register_rest_route( 'tegels', 'apppages', array(
        'methods' => 'GET',
        'callback' => 'get_apppages',
    ) );

} );


function get_timepaths( WP_REST_Request $request )
{
    $objects = [];

    $args = array (
        'post_type'             => 'planner_appbakkers',
        'post_status'           => 'publish',
        'order'					=> 'ASC',
        'numberposts'			=> -1,
        'post_per_page'			=> -1,
        'suppress_filters' 		=> 0
    );
    $query = new WP_Query( $args );
    global $post;

    while ( $query->have_posts() ): $query->the_post();

        $object = [];
        $object['name'] = get_the_title();
        $object['data'] = json_decode(get_post_meta($post->ID,'appbakkers_planner_json',true));


        $objects[] = $object;
    endwhile;

    return $objects;
}

function get_apppages( WP_REST_Request $request )
{
    $objects = [];

    $args = array (
        'post_type'             => 'pages_appbakkers',
        'post_status'           => 'publish',
        'order'					=> 'ASC',
        'numberposts'			=> -1,
        'post_per_page'			=> -1,
        'suppress_filters' 		=> 0
    );
    $query = new WP_Query( $args );
    global $post;

    while ( $query->have_posts() ): $query->the_post();

        $object = [];
        $object['name'] = get_the_title();
        $object['content'] = get_the_content();


        $objects[] = $object;
    endwhile;

    return $objects;
}

function get_tegels( WP_REST_Request $request )
{
    $objects = [];

    $args = array (
        'post_type'             => 'tegels',
        'order'					=> 'ASC',
        'numberposts'			=> -1,
                'showposts' => -1

    );
    $query = new WP_Query( $args );

    while ( $query->have_posts() ): $query->the_post();

        if(function_exists("get_field")) {

            global $post;

            if (get_the_title() != '' && get_field("tegel-foto-titel") != false) :

                $object = [];

                $object['id'] = $post->ID;
                $object['naam'] = get_the_title();
                $object['titel'] = get_field("tegel-foto-titel");
                $object['afbeelding'] = str_replace('.jpg', '-300x300.jpg', get_field("tegel_foto_afbeelding"));
                $object['onderwerp'] = get_field("tegel_foto_keynote");
                $object['tekst'] = get_field("tegel_foto_beschrijving");
                $object['youtube'] = get_field("tegel_foto_youtube");
                $object['start'] = get_field("tegel_aanvangstijd");
                $object['eind'] = get_field("tegel_eindtijd");
                $object['podium'] = get_field("tegel_podium");

                $objects[] = $object;
            endif;
        }
    else {
        $objects[] = ['Dependency niet geinstalleerd: advanced custom fields. zie regel 84 tm 92 in de plugin voor fields.'];
    }
    endwhile;

    return $objects;
}

function create_post_type() {
    register_post_type( 'pages_appbakkers',
        // CPT Options
        array(
            'labels' => array(
                'name' => __( 'App paginas' ),
                'add_new' => 'Nieuwe app pagina',
                'add_new_item' => 'Nieuwe app pagina toevoegen',
                'singular_name' => __( 'App Pagina' ),
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
    if(!post_type_exists('tegels'))
    {
        register_post_type( 'tegels',
            array(
                'labels' => array(
                    'name' => __( 'Tegels' ),
                    'singular_name' => __( 'Tegel' )
                ),
                'public' => true,
                'has_archive' => true,
            )
        );
    }
}

add_action( 'init', 'create_post_type' );
add_action( 'add_meta_boxes', 'add_metas' );
add_action('save_post', 'save_planner_appbakkers', 1, 2); // save the custom fields


function save_planner_appbakkers($post_id, $post)
{
    $rawjsondata = isset($_POST['jsondata'])? $_POST['jsondata'] : null;
    global $post;

    update_post_meta($post->ID, 'appbakkers_planner_json', $rawjsondata);

}

function add_metas()
{

}

function htmlplanner()
{
    global $post;
    $jsondata = get_post_meta($post->ID,'appbakkers_planner_json',true);
    ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            var items = <?php ($jsondata ? print $jsondata : print "[]"); ?>;

            $.each(items, function(i)
            {
                $('.removeme').hide();

                var li = $('<li/>')
                    .text(items[i][1])
                    .addClass('ui-state-default')
                    .data('id',items[i][0])
                    .appendTo('#sortable2');

            });
            $( "#sortable1 li" ).each(function( index ) {
                var content = $( this ).data('id');
                var thissy = $(this);
                $.each(items, function(i) {
                    if(content == items[i][0])
                    {
                        thissy.remove();
                    }
                });

            });
            $( "#sortable1, #sortable2" ).sortable({
                connectWith: ".connectedSortable",
                stop: function( event, ui ) {
                    changeHandler();
                }
            }).disableSelection();


            function changeHandler()
            {
                var target = $('#sortable2');
                var jsonfield = $('[name=jsondata]');
                var values = [];
                $( "#sortable2 li" ).each(function( index ) {
                    var content = $( this ).data('id');
                    if(content != null)
                    {
                        values.push([content,$( this ).text()]);
                    }
                });
                var jsonvalue = JSON.stringify(values);
                jsonfield.val(jsonvalue);
                if(target.children().length > 1)
                {
                    $('.removeme').hide();
                }
                else {
                    $('.removeme').show();
                }
            }
        } );
    </script>
    <style>
        #sortable1, #sortable2 {
            border: 1px solid #eee;
            width: 240px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }
        #sortable1 li, #sortable2 li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            font-size: 1.2em;
            width: 220px;
        }

        .wrapper {
            height:400px;
            overflow: auto;

        }
    </style>

    <div class="wrapper">

        <ul id="sortable1" class="connectedSortable">

            <?php
            query_posts(array(
                'post_type' => 'tegels',
                'showposts' => -1
            ) );
            ?>
            <?php while (have_posts()) : the_post();
                if(function_exists("get_field"))
                {
                    $time = get_field("tegel_aanvangstijd")." - ".get_field("tegel_eindtijd");
                }
                ?>
                <li class="ui-state-default" data-id="<?php print $post->ID;?>"><?php the_title(); print " ".$time; ?></li>

            <?php endwhile;?>


        </ul>

        <ul id="sortable2" class="connectedSortable">
            <li class="removeme">Geen events, sleep items hierheen om een pad te maken.</li>
        </ul>

        <input type="hidden" name="jsondata" value="<?php print htmlspecialchars($jsondata); ?>">
    </div>
    <?php
}
